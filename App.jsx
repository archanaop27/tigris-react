import './App.css'
import Header from './Header'
import Tiger from './Tiger'

function App(){
  return ( 
  <>
     <>
  <Header />
  <main>
    <section>
      <div className="container">
        <h1>"Tigris - A Haven for Tiger Conservation"</h1>
        <p>
          "Tigris is a digital sanctuary dedicated to the preservation of our
          majestic tiger population. Through advocacy, education, and actionable
          initiatives, we strive to safeguard these iconic creatures and their
          habitats. Join us in our mission to ensure a future where tigers roam
          freely, their presence an enduring symbol of nature's beauty and
          resilience."
        </p>
        <div className="buttonHolder">
          <a className="button button-primary " href="#">
            Book a safari
          </a>
          <a className=" button button-secondary" href="#">
            Contact
          </a>
        </div>
      </div>
    </section>
    <section id="section-about">
      <div className="container">
        <h2>"Our Commitment to Tiger Conservation"</h2>
        <p>
          "At Tigris, we're fervently committed to tiger conservation. Our
          mission is to protect these magnificent creatures by raising
          awareness, supporting conservation efforts, and advocating for their
          preservation. Through collaboration with experts and communities, we
          work tirelessly to ensure a thriving future for tigers in their
          natural habitats."
        </p>
      </div>
    </section>
    <section>
      <div className="container">
        <h2>Roars and insights</h2>
        <div className="bloglist">
      <Tiger />
      <Tiger />
      <Tiger />
      <Tiger />

        </div>
      </div>
    </section>
  </main>
  <footer />
</>
      
    </>
  )

}
export default App

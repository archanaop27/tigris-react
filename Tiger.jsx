function Tiger() {
    return (
        <article className="blog">
            <img src ="" alt="" />
            <h3>"Preserving Tigers: Our Urgent Mission"</h3>
            <p>
              "Explore our dedicated efforts towards preserving the majestic
              tigers of Tigris, as we strive to safeguard their habitat and
              ensure their survival."
            </p>
          </article>
    )

    }
export default Tiger
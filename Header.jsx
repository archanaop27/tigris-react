function Header() {
    return (
        <header>
    <div className="container">
      <span id="logo">Tigris</span>
      <button id="menuButton">
        <span className="material-symbols-outlined">pets</span>
      </button>
      <nav>
        <ul id="navlist">
          <li>
            <a href="#">Home</a>
          </li>
          <li>
            <a href="#">About</a>
          </li>
          <li>
            <a href="#">Services</a>
          </li>
          <li>
            <a href="#">Contact</a>
          </li>
        </ul>
      </nav>
    </div>
  </header>
    )
}

export default Header